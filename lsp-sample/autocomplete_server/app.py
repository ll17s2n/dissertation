from flask import Flask, request, jsonify
import json
import threading

from transformers import GPT2LMHeadModel, GPT2Tokenizer
import torch

# from interact import get_predictions, load_model_and_tokenizer
import time

# from labml import monit
# from python_autocomplete.evaluate.beam_search import NextWordPredictionComplete
# from python_autocomplete.evaluate.factory import get_predictor



def load_model_and_tokenizer():
    model_path = "tmp/model/distilgpt2_fine_tuned_coder/0_GPTSingleHead/"
    # load fine-tunned model and tokenizer from path
    model = GPT2LMHeadModel.from_pretrained(model_path)
    tokenizer = GPT2Tokenizer.from_pretrained(model_path)
    model.eval()
    if torch.cuda.is_available():
        model.to("cuda")
    return model, tokenizer



def get_predictions(context, model, tokenizer):
    input_ids = tokenizer.encode("<python> " + context, return_tensors='pt')

    outputs = model.generate(input_ids=input_ids.to("cuda") if torch.cuda.is_available() else input_ids,
                                max_length=128,
                                temperature=0.7,
                                num_return_sequences=1)

    output_decoded = tokenizer.decode(outputs[0], skip_special_tokens=True)
    return output_decoded

def get_next_token_predictions(context, model, tokenizer):
	input_ids = tokenizer.encode("<python> " + context, return_tensors='pt')

	outputs = model(input_ids)
	logits = outputs.logits[:, -1, :]

	pred_id = torch.argmax(logits).item()

	output_decoded = tokenizer.decode(pred_id, skip_special_tokens=True)
	return output_decoded


app = Flask(__name__)
model, tokenizer = load_model_and_tokenizer()

lock = threading.Lock()

@app.route('/', methods=['POST'])
def autocomplete():
	context = json.loads(request.data.decode("utf-8"))["prompt"]
	print("context; ", context)
	acquired = lock.acquire(blocking=False)
	if acquired:
		start = time.time()
		predictions = get_next_token_predictions(context, model, tokenizer)
		end = time.time()
		print(end - start)
		print("prediction: ", predictions)
		lock.release()
		return jsonify({'success': True, 'predictions': predictions})
	else:
		return jsonify({'success': False})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5050, debug=True)




