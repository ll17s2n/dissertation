from transformers import GPT2LMHeadModel, GPT2Tokenizer
import argparse
import torch
# from torch.utils.data import DataLoader  
import time
import os

from datasets import load_dataset
from tqdm import tqdm

import matplotlib.pyplot as plt

import math

model_path = "tmp/model/distilgpt2_fine_tuned_coder/0_GPTSingleHead/"
model = GPT2LMHeadModel.from_pretrained(model_path)
tokenizer = GPT2Tokenizer.from_pretrained(model_path)
model.eval()
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
dataset = load_dataset('json', data_files={'test': "source_code/json/eval.jsonl"})
python_token = tokenizer.encode(["<python> "])



def accuracy_across_all_examples(): #Implement plotting, maybe worth using Jupyter
	plt.ylabel('Average Accuracy (Percentage)')
	plt.xlabel('Number of Examples')
	sequence_sizes = {
		100: "r",
	}

	for size in sequence_sizes:
		example_count = 0
		x = [] #array to store x values for plot
		y = [] #array to store y values for plot
		list_of_accuracies = []
		for line in dataset["test"]["token_ids"]:
			example_count = example_count + 1
			line = python_token + line
			# print(tokenizer.decode(line))
			encodings = tokenizer("\n\n".join(tokenizer.decode(line)), return_tensors="pt")
			print("size:", encodings.input_ids.size(1))
			hit = 0
			miss = 0
			counter = 0
			accuracy = 0
			colour = sequence_sizes[size]
			for i in range(1, encodings.input_ids.size(1), size):
				if i + size > encodings.input_ids.size(1): #input size is big enough
					break
				# 	continue

				# begin_loc = max(i + stride - max_length, 0)
				# end_loc = min(i + stride, encodings.input_ids.size(1))
				# trg_len = end_loc - i  # may be different from stride on last loop
				# input_ids = encodings.input_ids[:, begin_loc:end_loc].to(device)
			
				input_ids = encodings.input_ids[:, i:i+size].to(device)
				target_id = encodings.input_ids[:, i+1]

				outputs = model(input_ids)
				logits = outputs.logits[:, -1, :]

				pred_id = torch.argmax(logits).item()
				
				if pred_id == target_id:
					hit = hit + 1
				else:
					miss = miss + 1
				
				counter = counter + 1

				accuracy = hit / counter * 100
				
				list_of_accuracies.append(accuracy)

				# if accuracy != prev_accuracy:
				# 	x.append(example_count)
				# 	y.append(accuracy)
				# 	# plt.plot(len(input_ids[0]), [accuracy], colour)

				# prev_accuracy = accuracy
			if example_count % 100 == 0:
				if len(list_of_accuracies) > 0:
					average_accuracy = sum(list_of_accuracies) / len(list_of_accuracies)
					y.append(average_accuracy)
					x.append(example_count)
					list_of_accuracies = []

			if example_count == 5001:
				break

			print(example_count)
		
		plt.plot(x, y, colour)

	
	plt.show()






def accuracy_sequence(): #Implement plotting, maybe worth using Jupyter
	plt.ylabel('Accuracy (Percentage)')
	plt.xlabel('Sequence size')
	colours = {
		1: "b",
		2: "g",
		3: "r",
		4: "c",
		5: "y"
	}
	
	overall = 0
	number_of_lines = 0
	for line in dataset["test"]["token_ids"]:
		line = python_token + line
		print(tokenizer.decode(line))
		encodings = tokenizer("\n\n".join(tokenizer.decode(line)), return_tensors="pt")
		hit = 0
		miss = 0
		counter = 0
		prev_accuracy = 0
		accuracy = 0
		number_of_lines = number_of_lines + 1
		colour = colours[number_of_lines]
		x = [] #array to store x values for plot
		y = [] #array to store y values for plot
		for i in range(1, len(line)-1):
			# input_ids =  python_token + line[:i+1]
			# target_output = line[i+1]

			input_ids = encodings.input_ids[:, 0:i].to(device)
			target_id = encodings.input_ids[:, i+1]
			# print(tokenizer.decode(target_id))
			# print(input_ids[:, -1:], target_id)
			# exit()

			# exit()

			# decoded_input = tokenizer.decode(inputs_ids)
			# encodings = tokenizer.encode(decoded_input)

			# print(line)
			# print(input_ids)
			# print(target_output)
			outputs = model(input_ids)
			logits = outputs.logits[:, -1, :]

			pred_id = torch.argmax(logits).item()
			
			if pred_id == target_id:
				hit = hit + 1
			else:
				miss = miss + 1
			
			counter = counter + 1
			overall = overall + 1
				# print(pred_id, target_id)
				# print(tokenizer.decode(target_id))
				
				# exit()
			print(counter, hit, miss)
			accuracy = hit / counter * 100

			if accuracy != prev_accuracy:
				x.append(len(input_ids[0]))
				y.append(accuracy)
				# plt.plot(len(input_ids[0]), [accuracy], colour)


			prev_accuracy = accuracy
			# if i == 100:
			# 	break

		plt.plot(x, y, colour)	
		
		if number_of_lines == 5:
			break #accuracy is around 40%

	
	plt.show()

	print(counter, hit, miss, hit + miss)

		# for element in line:
		#     input_ids.append()
		#     outputs = model(input_ids, labels=target_ids)
		# print(line)


def perplexity(): #Graph this, not sure if this works as ppl is so high
	# data = []
	# count = 0
	# for element in dataset["test"]["token_ids"]:
	# 	for i in element:
	# 		data.append(i)
	# 	if count == 1: #number of how many python files/functions
	# 		break
	# 	count = count + 1

	plt.ylabel('Perplexity ')
	plt.xlabel('Example Number (Sequence Size = 128)')
	max_length = model.config.n_positions
	stride = 128
	example_count = 0
	colour = "r"

	x = [] #array to store x values for plot
	y = [] #array to store y values for plot
	for line in dataset["test"]["token_ids"]:
		example_count = example_count + 1
		encodings = tokenizer("\n\n".join(tokenizer.decode(line)), return_tensors="pt")
		nlls = []
		for i in tqdm(range(0, encodings.input_ids.size(1), stride)):
			begin_loc = max(i + stride - max_length, 0)
			end_loc = min(i + stride, encodings.input_ids.size(1))
			trg_len = end_loc - i  # may be different from stride on last loop
			input_ids = encodings.input_ids[:, begin_loc:end_loc].to(device)
			target_ids = input_ids.clone()
			target_ids[:, :-trg_len] = -100

			with torch.no_grad():
				outputs = model(input_ids, labels=target_ids)
				# print("vocab size", outputs.logits.size())
				# print("loss: ", math.exp(outputs.loss))
				neg_log_likelihood = outputs[0] * trg_len
				# print("neg: ", neg_log_likelihood)

			nlls.append(neg_log_likelihood)

		ppl = torch.exp(torch.stack(nlls).sum() / end_loc)
		x.append(example_count)
		y.append(ppl)
		
		if example_count == 100:
			break

	plt.plot(x, y, colour)
	plt.show()

def accuracy_sequence_time(): #Implement plotting, maybe worth using Jupyter
	plt.ylabel('Time Taken (Seconds)')
	plt.xlabel('Sequence size')
	colours = {
		1: "b",
		2: "g",
		3: "r",
		4: "c",
		5: "y"
	}

	number_of_lines = 0
	for line in dataset["test"]["token_ids"]:
		line = python_token + line
		encodings = tokenizer("\n\n".join(tokenizer.decode(line)), return_tensors="pt")
		number_of_lines = number_of_lines + 1
		colour = colours[number_of_lines]
		x = [] #array to store x values for plot
		y = [] #array to store y values for plot
		for i in range(1, len(line)-1):
			input_ids = encodings.input_ids[:, 0:i+1].to(device)
			target_id = encodings.input_ids[:, i+1]

			start = time.time()
			outputs = model(input_ids)
			end = time.time()
			time_taken = end - start

			x.append(len(input_ids[0]))
			y.append(time_taken)
			print(len(input_ids[0]), time_taken)

		plt.plot(x, y, colour)	
		
		if number_of_lines == 1:
			break

	
	plt.show()






def most_common_tokens(): #matlab bar chart
	most_common_correct_tokens = {}
	most_common_incorrect_tokens = {}

	# plt.ylabel('Average Accuracy (Percentage)')
	# plt.xlabel('Number of Examples')
	size = 1
	# implement stride
	example_count = 0
	x = [] #array to store x values for plot
	y = [] #array to store y values for plot
	for line in dataset["test"]["token_ids"]:
		example_count = example_count + 1
		line = python_token + line
		encodings = tokenizer("\n\n".join(tokenizer.decode(line)), return_tensors="pt")
		hit = 0
		miss = 0
		counter = 0
		# colour = 'r'
		for i in range(1, len(line)-1):
			if i < size: #input size is big enough
				continue
		
			input_ids = encodings.input_ids[:, i-size:i].to(device)
			target_id = encodings.input_ids[:, i+1]

			outputs = model(input_ids)
			logits = outputs.logits[:, -1, :]

			pred_id = torch.argmax(logits).item()
			
			if pred_id == target_id:
				hit = hit + 1
				decoded_token = tokenizer.decode(target_id)
				if decoded_token not in most_common_correct_tokens:
					most_common_correct_tokens.setdefault(decoded_token, 1)
				else:
					most_common_correct_tokens[decoded_token] = most_common_correct_tokens[decoded_token] + 1
			else:
				miss = miss + 1
				decoded_token = tokenizer.decode(target_id)
				if decoded_token not in most_common_incorrect_tokens:
					most_common_incorrect_tokens.setdefault(decoded_token, 1)
				else:
					most_common_incorrect_tokens[decoded_token] = most_common_incorrect_tokens[decoded_token] + 1
			
			counter = counter + 1

		print(counter, hit, miss)
		print(example_count)
		if example_count == 101:
			break
		
	
	# sorts the dictionaries and picks the top three.
	correct_maximum_three = sorted(most_common_correct_tokens.items(), key=(lambda item: item[1]), reverse=True)[:5]
	incorrect_maximum_three = sorted(most_common_incorrect_tokens.items(), key=(lambda item: item[1]), reverse=True)[:5]

	correct_tokens = []
	correct_count = []
	for element in correct_maximum_three:
		correct_tokens.append(repr(element[0]))
		correct_count.append(element[1])
	
	incorrect_tokens = []
	incorrect_count = []
	for element in incorrect_maximum_three:
		incorrect_tokens.append(repr(element[0]))
		incorrect_count.append(element[1])

	figure1 = plt.figure(1)
	plt.xlabel('Tokens')
	plt.ylabel('Number of Correct Predictions \n (100 Examples)')
	plt.bar(correct_tokens,correct_count)
	figure2 = plt.figure(2)
	plt.xlabel('Tokens')
	plt.ylabel('Number of Inorrect Predictions \n (100 Examples)')
	plt.bar(incorrect_tokens,incorrect_count)
	# show both figures
	plt.show()






function_dictionary = {
	"accuracy_sequence": accuracy_sequence,
	"accuracy_sequence_time": accuracy_sequence_time,
	"perplexity": perplexity,
	"accuracy_across_all_examples": accuracy_across_all_examples,
	"most_common_tokens": most_common_tokens,
	"accuracy_across_all_examples": accuracy_across_all_examples
}

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Parameters')
	parser.add_argument('--function', type=str, default="accuracy_sequence",
						help='accuracy_sequece, accuracy_sequence_time, perplexity')

	args = parser.parse_args()

	function_to_run = function_dictionary[args.function]

	function_to_run()



